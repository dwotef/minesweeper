/*
--- MINESWEEPER ---
VERSION:    1.0
AUTHOR:     dwotef
DATE:       12-29-2023
-------------------
*/

#include <ncurses.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#define ROWS 14
#define COLS 14
#define MINES 50

typedef struct {
    int ch;
    int flag;
    bool mine;
    bool rev;
} tile;

typedef struct {
    int y;
    int x;
} coords;

coords kb_input(coords cursor, tile field[ROWS][COLS]);
void reveal_tile(int y, int x, tile field[ROWS][COLS]);
void display_field(WINDOW* game_win, tile field[ROWS][COLS]);
void init_field(tile field[ROWS][COLS]);
int prox_check(int x, int y, tile field[ROWS][COLS]);
void init_colors();
int victory_check(tile field[ROWS][COLS]);

int main() {
    initscr();
    noecho();

    init_colors();

    // init time
    srand(time(NULL));

    // generate field
    tile field[ROWS][COLS];
    init_field(field);

    // game window init
    WINDOW* game_win = newwin(ROWS + 2, COLS + 2, 0, 0);
    box(game_win, 0, 0);

    // score window init
    WINDOW* score_win = newwin(3, COLS + 2, ROWS + 2, 0);
    box(score_win, 0, 0);
    refresh();
    wrefresh(score_win);

    // init cursor
    coords cursor = {(ROWS / 2), (COLS / 2)};
    move(cursor.y + 1, cursor.x + 1);

    while (1) {
        display_field(game_win, field);
        cursor = kb_input(cursor, field);
        if (victory_check(field) == 1) {
            wattron(score_win, COLOR_PAIR(10));
            mvwprintw(score_win, 1, 1, "YOU WIN!");
            wattroff(score_win, COLOR_PAIR(10));
            wrefresh(score_win);
            wattron(game_win, COLOR_PAIR(10));
            mvwprintw(game_win, 0, (COLS / 2 - 3), "YOU WIN!");
            wattroff(game_win, COLOR_PAIR(10));
        }
        /*
        mvprintw(24, 2, "       ");
        int c = getch();
        mvprintw(24, 2, "%d", c);
        */
    }

    endwin();
    return 0;
}

coords kb_input(coords cursor, tile field[ROWS][COLS]) {
    int input = getch();
    switch(input) {
        case 'a':
        case 'h':       // left
            cursor.x--;
            break;
        case 's':
        case 'j':       // down
            cursor.y++;
            break;
        case 'w':
        case 'k':       // up
            cursor.y--;
            break;
        case 'd':
        case 'l':       // right
            cursor.x++;
            break;
        case 'f':       // flag
            if (field[cursor.y][cursor.x].flag < 2) {
                field[cursor.y][cursor.x].flag++;
            } else {
                field[cursor.y][cursor.x].flag = 0;
            }
            break;
        case 10:        // enter
        case 32:        // space
            if (field[cursor.y][cursor.x].flag == 0 && field[cursor.y][cursor.x].rev != true) {
                reveal_tile(cursor.y, cursor.x, field);
                // temporary until gameover is implemented
                if (field[cursor.y][cursor.x].mine == true) {
                    for (int i = 0; i < ROWS; i++) {
                        for (int j = 0; j < COLS; j++) { 
                            field[i][j].rev = true;
                        }
                    }
                }
            }
        default:
            break;
    }
    move(cursor.y + 1, cursor.x + 1);
    return cursor;
}

void reveal_tile(int y, int x, tile field[ROWS][COLS]) {
    // check if y x is a mine
    //
    int ry, rx;
    if (field[y][x].ch == '0') {
    for (int i = -1; i <= 1; i++) {
        ry = y + i;
        for (int j = -1; j <= 1; j++) {
            rx = x + j;
            if (ry < ROWS && ry >= 0 && rx < COLS && rx >= 0) { 
                if (field[ry][rx].ch == '0' && field[ry][rx].rev != true) {
                    field[ry][rx].rev = true;
                    reveal_tile(ry, rx, field);
                } else {
                    field[ry][rx].rev = true;
                }
            }
        }
    }
    } else {
        field[y][x].rev = true;
    }
}

void display_field(WINDOW* game_win, tile field[ROWS][COLS]) {
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) { 
            if (field[i][j].rev == true) {
                if (field[i][j].ch == '0') {
                    mvwprintw(game_win, i + 1, j + 1, " ");
                } else if (field[i][j].ch == '*') {
                    wattron(game_win, COLOR_PAIR(4));
                    mvwprintw(game_win, i + 1, j + 1, "*");
                    wattroff(game_win, COLOR_PAIR(4));
                } else { 
                    wattron(game_win, COLOR_PAIR(field[i][j].ch));
                    mvwprintw(game_win, i + 1, j + 1, "%c", field[i][j].ch);
                    wattroff(game_win, COLOR_PAIR(field[i][j].ch));
                }
            } else if (field[i][j].flag > 0) {
                wattron(game_win, COLOR_PAIR(3));
                if (field[i][j].flag == 1) {
                    mvwprintw(game_win, i + 1, j + 1, "F");
                } else if (field[i][j].flag == 2) {
                    mvwprintw(game_win, i + 1, j + 1, "?"); 
                }
                wattroff(game_win, COLOR_PAIR(3));
            } else {
                wattron(game_win, COLOR_PAIR(2));
                mvwprintw(game_win, i + 1, j + 1, "#");
                wattroff(game_win, COLOR_PAIR(2));
            }
        }
    }
    wrefresh(game_win);
}

void init_field(tile field[ROWS][COLS]) {
    // empty tiles
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) { 
            field[i][j].ch = '0';
            field[i][j].flag = 0;
            field[i][j].mine = false;
            field[i][j].rev = false;
        }
    }
    // generate mines
    for (int i = 0; i < MINES; i++) {
         int rand_y = rand() % (ROWS + 1);
         int rand_x = rand() % (COLS + 1);
         field[rand_y][rand_x].mine = true;
         field[rand_y][rand_x].ch = '*';
    }

    int mine_count;
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            if (field[i][j].mine == false) {
                mine_count = prox_check(i, j, field);
                field[i][j].ch = mine_count + '0';
            }
        }
    }
}

int prox_check(int y, int x, tile field[ROWS][COLS]) {
    int ry, rx;
    int counter = 0;
    for (int i = -1; i <= 1; i++) {
        ry = y + i;
        for (int j = -1; j <= 1; j++) {
            rx = x + j;
            if (ry < ROWS && ry >= 0 && rx < COLS && rx >= 0) { 
                if (field[ry][rx].mine == true) {
                    counter++;
                }
            }
        }
    }
    return counter;
}

void init_colors() {
    start_color();

    init_color(COLOR_BLACK, 20, 10, 0);
    init_color(COLOR_WHITE, 200, 200, 200);
    /*
    init_color(COLOR_BLUE, 150, 550, 999);
    init_color(COLOR_GREEN, 100, 999, 500);
    init_color(COLOR_RED, 999, 300, 200);
    init_color(COLOR_YELLOW, 999, 900, 200);
    init_color(COLOR_MAGENTA, 900, 200, 100);
    init_color(COLOR_CYAN, 0, 300, 900);
    */

    init_pair(1, COLOR_BLACK, COLOR_WHITE);
    init_pair(2, COLOR_BLACK, COLOR_WHITE);         // unrevealed tile
    init_pair(3, COLOR_YELLOW, COLOR_WHITE);        // flag
    init_pair(4, COLOR_BLACK, COLOR_RED);           // mine
    init_pair(10, COLOR_GREEN, COLOR_BLACK);        // 
    init_pair(49, COLOR_BLUE, COLOR_BLACK);         // 1
    init_pair(50, COLOR_GREEN, COLOR_BLACK);        // 2
    init_pair(51, COLOR_RED, COLOR_BLACK);          // 3
    init_pair(52, COLOR_YELLOW, COLOR_BLACK);       // 4
    init_pair(53, COLOR_MAGENTA, COLOR_BLACK);      // 5
    init_pair(54, COLOR_CYAN, COLOR_BLACK);         // 6
    init_pair(55, COLOR_BLUE, COLOR_BLACK);         // 7


}

int victory_check(tile field[ROWS][COLS]) {
    int victory = 1;
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            if (field[i][j].rev == false && field[i][j].mine == false) {
                victory = 0;
            }
            if (victory == 0) break;
        }
        if (victory == 0) break;
    }
    return victory;
}



            
